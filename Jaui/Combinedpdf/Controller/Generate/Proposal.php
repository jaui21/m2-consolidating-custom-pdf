<?php
namespace Jaui\Combinedpdf\Controller\Generate;

use Magento\Framework\App\Action\Action;

class Proposal extends Action
{

    protected $customerSession;
    protected $wishlist;
    protected $productCollectionFactory;
    protected $productRepositoryFactory;   
    protected $fileFactory;
 

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Wishlist\Model\Wishlist $wishlist,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,        
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory) 
    {
        $this->customerSession = $customerSession;
        $this->wishlist = $wishlist;
        $this->productRepositoryFactory = $productRepositoryFactory;
        $this->productCollectionFactory = $productCollectionFactory;    	
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
    }
 
    /**
     * to generate pdf
     *
     * @return void
     */
    public function execute()
    {
        $customer_id = $this->customerSession->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$wishlist = $objectManager->get('\Magento\Wishlist\Model\Wishlist');
		$wishlist_collection = $wishlist->loadByCustomerId($customer_id, true)->getItemCollection();

           //*****************************
         //  Query direct to database 
        //*****************************
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();       
        //$s = count($wishlist_collection);
        $t = 1;
        $pdf = new \Zend_Pdf();
        $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $page = $pdf->pages[0]; // this will get reference to the first page.
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontbold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font,15);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $x = 30;
        $pageTopalign = 850; //default PDF page height
        $this->y = 850 - 100; //print table row from page top – 100px

        foreach ($wishlist_collection as $item) { 

        $productId = $item->getProduct()->getId();
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addIdFilter($productId);


            foreach ($productCollection as $product) { 
            
            $select = $connection->select()
                ->from('tm_brand','logo')
                ->where('brand_id =?', $product->getData('brand_id'));
            $brand_file = $connection->fetchOne($select);

            $pg[$t] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
            $pdf->pages[] = $pg[$t];
            //$page = $pdf->pages[]; // this will get reference to the first page.
            $style = new \Zend_Pdf_Style();
            $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
            $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
            $fontbold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
            $style->setFont($font,15);
            $pg[$t]->setStyle($style);
            $width = $pg[$t]->getWidth();
            $hight = $pg[$t]->getHeight();
            $x = 30;
            $pageTopalign = 850; //default PDF page height
            $this->y = 850 - 100; //print table row from page top – 100px

            $rootPath = "/home/webuser/public_html";

            // Product Specification Sheet    
    		$orangeLine =  \Zend_Pdf_Image::imageWithPath($rootPath . "/pub/specsheet/images/product-specification-sheet.png" );
            $pg[$t]->drawImage($orangeLine, 30 , -100 , 70, 700);

            // Product Brand Logo
            $brandImagefile = $rootPath . "/pub/media/brandproductpage/brandproductpage/specsheet/" . $brand_file;
            if(file_exists($brandImagefile)):    
            $brandImage =  \Zend_Pdf_Image::imageWithPath($brandImagefile);
            $pg[$t]->drawImage($brandImage, $x + 70 , $this->y-270, 250, $this->y -120);
            endif;

            // Product Name    
            $style->setFont($font,18);
            $pg[$t]->setStyle($style);
            $pg[$t]->drawText(__("%1", $product->getName()), $x + 70, $this->y - 250 , 'UTF-8');

            // Product Code
            $style->setFont($fontbold,12);
            $pg[$t]->setStyle($style);        
            $pg[$t]->drawText(__("%1", $product->getSku()), $x + 70, $this->y -270, 'UTF-8');
     
            // Product Image
            $productImagefile = $rootPath . "/pub/media/catalog/product/" . $product->getImage();

            if(file_exists($productImagefile)):
                $productImage =  \Zend_Pdf_Image::imageWithPath($productImagefile);
                list($width, $height) = getimagesize($productImagefile);
                if($height > $width) {
                    $pg[$t]->drawImage($productImage, 110 , $this->y -600, 330, $this->y - 300); // portrait orientation
                } else {
                    $pg[$t]->drawImage($productImage, 80 , $this->y -540, 390, $this->y - 320);  // landscape orientation
                }
            endif;

            // Product Dimension    
            $style->setFont($fontbold,12);
            $pg[$t]->setStyle($style); 
            $pg[$t]->drawText(__("Dimension:"), $x + 370, $this->y-340, 'UTF-8');
            $style->setFont($font,11);
            $pg[$t]->setStyle($style);
            $dimension = $this->getDimension($product->getId());
            $pg[$t]->drawText(__("%1",  $dimension), $x + 370, $this->y-360, 'UTF-8');

            // Product Warranty    
            $style->setFont($fontbold,12);
            $pg[$t]->setStyle($style); 
            $pg[$t]->drawText(__("Warranty:"), $x + 370, $this->y-380, 'UTF-8');
            $style->setFont($font,11);
            $pg[$t]->setStyle($style); 
            $warranty =  $this->getWarranty($product->getId()); 
            $warrantylabel =  $this->getWarrantylabel($product->getId(), $warranty);
            if(!empty($warranty)):    
            $pg[$t]->drawText(__("%1", $warrantylabel), $x + 370, $this->y-400, 'UTF-8');
            endif;

            // Product Features
            $style->setFont($fontbold,12);
            $pg[$t]->setStyle($style); 
            $pg[$t]->drawText(__("Features:"), $x + 370, $this->y-420, 'UTF-8');
            $featureY = $this->y - 440;
            $featureYB = $this->y - 436;
            $style->setFont($font,11);
            $pg[$t]->setStyle($style);        
            //$description = wordwrap($product->getShortDescription(), 90, "</li>");
            $description = $product->getShortDescription();
            
            $arrdescription = explode("</li>", $description); 
            $count = count($arrdescription);
            foreach (explode("</li>", $description) as $i => $line) {

                if (--$count <= 0) { break; }    
                $pg[$t]->drawCircle($x + 375, $featureYB - $i * 18, 2); 
                $pg[$t]->drawText(__("  %1", strip_tags($line)) , $x + 375, $featureY - $i * 18, 'UTF-8');
            }

            // Black Rectangle   
            $style->setFillColor(new \Zend_Pdf_Color_Html('#BBBBBB'));
            $pg[$t]->setStyle($style);
            $pg[$t]->drawRectangle(0, $this->y -690, 29, $this->y - 750, \Zend_Pdf_Page::SHAPE_DRAW_FILL);
            $pg[$t]->drawRectangle(71, $this->y -690, $pg[$t]->getWidth(), $this->y - 750, \Zend_Pdf_Page::SHAPE_DRAW_FILL);

            // Footer Note
            $style->setFont($font,8);
            $pg[$t]->setStyle($style);
            $pg[$t]->drawText(__("NOTE: Images, specifications and dimension correct at the time of publishing. It is advised that these details be"), 220, $this->y-670);
            $pg[$t]->drawText(__("confirmed prior to construction as no liability will be taken by Harvey Norman Commercial for any changes in specifications."), 200, $this->y-680);

            // Harvey Norman Logo    
            $imagehncffe =  \Zend_Pdf_Image::imageWithPath($rootPath . "/pub/specsheet/images/harveynorman-ffe-white.png");
            $pg[$t]->drawImage($imagehncffe, 350 , $this->y -733, 500, $this->y - 703);
            
     		} // End Product Collection	
		$t++;
        } // End Wishlist Collection

        $fileName = 'customer_' . $customer_id . '_proposal.pdf';
        $pdf->save("/home/webuser/public_dev/pub/specsheet/combinedpdf/proposal/" .  $fileName );

        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $this->_redirect( $baseUrl . 'specsheet/combinedpdf/proposal/' . $fileName);
    }
    public function getDimension($id) 
    {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($id);

            return $_product->getDimension();
    }
    public function getWarranty($id) 
    {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($id);

            return $_product->getWarranty();
    } 
    public function getWarrantylabel($id, $optionid) 
    {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($id);

            $attr = $_product->getResource()->getAttribute('warranty');
             if ($attr->usesSource()) {
                   $optionText = $attr->getSource()->getOptionText($optionid);
             }
            return $optionText;
    }                
}